## Example 1: Square Moon

### Overview
This examples demonstates using background image and a single block type (no border) with 3 hitpoints and diffrent colors for every amount of hitpoints left.

####level_definition.txt
```
START_LEVEL
level_name:Square Moon
ball_velocities:45,500
background:image(background_images/night.jpg)
paddle_speed:650
paddle_width:160
block_definitions:definitions/moon_block_definitions.txt
blocks_start_x:25
blocks_start_y:80
row_height:100
num_blocks:4
START_BLOCKS
--ll--
--ll--
END_BLOCKS
END_LEVEL
```
####moon_block_definitions.txt
```
#block definitions
bdef symbol:l width:100 height:100 hit_points:3 fill-3:color(RGB(244,248,129)) fill-2:color(RGB(205,208,112)) fill-1:color(RGB(154,157,84))

#spacers definitions
sdef symbol:- width:30

```
####Screenshot
![Square Moon Screenshot](ass6/square_moon.png)


## Example 2: Welcome to the Jungle

#### Overview
This examples demonstates using images as block and the use of defaults in the block defintion file.

####level_definition.txt
```
START_LEVEL
level_name:Welcome to the Jungle
ball_velocities:45,500 -45,500 -30,500 30,500
background:image(background_images/jungle.jpg)
paddle_speed:650
paddle_width:160
block_definitions:definitions/jungle_block_definitions.txt
blocks_start_x:25
blocks_start_y:80
row_height:25
num_blocks:26
START_BLOCKS
-
-
-
-
-lllllllllllll--
-zzzzzzzzzzzzz--
END_BLOCKS
END_LEVEL
```
####jungle_block_definitions.txt
```
# default values for blocks
default height:25 width:50 stroke:color(black)

# block definitions
bdef symbol:z hit_points:1 fill:image(block_images/zebra.jpg)
bdef symbol:l hit_points:2 fill:image(block_images/zebra.jpg) fill-2:image(block_images/leopard.jpg)

# spacers definitions
sdef symbol:- width:50

```
####Screenshot
![Screenshot](ass6/jungle.png)

## Example 3: Retro Arrows

#### Overview
This examples demonstrates the use of many types of blocks and a complex layout.

####level_definition.txt
```
START_LEVEL
level_name: Retro Arrows
ball_velocities:45,500 46,500 47,500
background:color(RGB(142,0,0))
paddle_speed:650
paddle_width:250
block_definitions:definitions/arrows_block_definitions.txt
blocks_start_x:25
blocks_start_y:80
row_height:25
num_blocks:135
START_BLOCKS
-
-
ggggggggggggggg
gyyyryygoobooog
gyyrryygoobboog
gyrrrrrgbbbbbog
grrrrrrgbbbbbbg
gyrrrrrgbbbbbog
gyyrryygoobboog
gyyyryygoobooog
ggggggggggggggg

END_BLOCKS
END_LEVEL
```
####arrows_block_definitions.txt
```
# default values for blocks
default height:25 width:50 stroke:color(black) hit_points:1

# block definitions
bdef symbol:g hit_points:2 fill:color(lightGray) fill-2:color(gray)
bdef symbol:b fill:color(blue)
bdef symbol:y fill:color(yellow)
bdef symbol:r fill:color(green)
bdef symbol:o fill:color(orange)


# spacers definitions
sdef symbol:- width:50

```
####Screenshot
![Screenshot](ass6/retro_arrows.png)

## Example 4: Final Four (V2)

#### Overview
This examples demonstrates the translation of a level we created in the previous assignment to the new format.

####level_definition.txt
```
START_LEVEL
level_name: Final Four (V2)
ball_velocities:45,500
background:image(background_images/clouds.png)
paddle_speed:650
paddle_width:160
block_definitions:definitions/final_four_block_definitions.txt
blocks_start_x:25
blocks_start_y:80
row_height:25
num_blocks:105
START_BLOCKS
-
-
GGGGGGGGGGGGGGG
rrrrrrrrrrrrrrr
yyyyyyyyyyyyyyy
ggggggggggggggg
wwwwwwwwwwwwwww
ppppppppppppppp
ccccccccccccccc

END_BLOCKS
END_LEVEL
```
####final_four_block_definitions.txt
```
# default values for blocks
default height:25 width:50 stroke:color(black) hit_points:1

# block definitions
bdef symbol:G hit_points:2 fill:color(lightGray) fill-2:color(gray)
bdef symbol:b fill:color(blue)
bdef symbol:y fill:color(yellow)
bdef symbol:r fill:color(red)
bdef symbol:g fill:color(green)
bdef symbol:p fill:color(pink)
bdef symbol:w fill:color(white)
bdef symbol:c fill:color(cyan)

# spacers definitions
sdef symbol:- width:50

```
####Screenshot
![Screenshot](ass6/final_four.png)


### Resource pack

The resource pack used for the examples can be downloaded: [exampleResources.zip](ass6/exampleResources.zip)

