## Working with Multiple Files

In Java, each public class is resides in its own file.
As each program is usually composed of many classes, this means you will have many different files.

### Compiling:

In order to compile the files A.java, B.java and C.java, you can use the command:
```bash
> javac A.java B.java C.java
```

Alternatively, in order to compile all the files with the extension `.java` in the current directory, you can use:
```bash
> javac *.java
```

Compiling like is done above will put the compiled `.class` files alongside the source `.java` files.
It is sometimes convenient to separate the compiled files from the source files.
You can use the `-d` switch to tell the compiler to put the class files in a specific folder.
```bash
> javac -d ../class *.java
```
will tell the compiler to create the `.class` files in the `../class` directory.

### Running

When you run a java program, you tell the java interpreter to run the `void main(String[] args)` method of a specific class.

For example, the commandline:
```bash
> java A
```

tells java to look at the `A` class (which is in the file `A.class`) and run its `main(String[] args)` method.

When doing its work, class `A` may want to create or send messages to other classes (for example `B` and `C`).  This is not a problem, as long as java knows where the `B.class` and `C.class` files are.  By default, java looks for the class files in the current directory.  You can tell java (and javac) to look at other directories as well, using the `-cp` commandline switch.


