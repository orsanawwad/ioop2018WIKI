**These instructions are required for ALL the code you submit**

Please follow them completely, or we will not be able to grade you code, and you will receive a 0 grade for the assignment.

* Use the [submit](http://submit.cs.biu.ac.il/) system.
* All your code should reside in a single `.zip` file named `assN.zip` where `N` is the assignment number.
* When the zip file is extracted, it should create a directory called `src`. All your other code should reside under the `src` directory. You can create other directories if you want.
* The main level of your zip (one above the `src` directory) should include a `makefile` with at least two targets:
   * `compile` will compile the code
      * The code should be compiled into a `bin` directory, residing alongside `src`. To achieve that, you can use the `-d` flag. For example, you can use `javac -d bin src/TheFile.java`
   * `run` will run the main class
      * In case there are several tasks with runnable parts, you should have one target for each task, called runX or runX-Y where X and X-Y are the task numbers. For example `run2` or `run2-1`.
      * In case the task requires commandline parameters, your run target should supply parameters with reasonable values.  We may later run your code with other values.
* We may ask you to call certain classes in certain names. Please do so.


We should be able to run the following commands (on unix) to run your code:
```bash
unzip ass1.zip
make compile
make run
```

**TIP** you may want to add a `check` target to your makefile, that will run [checkstyle](CodingStyle) on all your code, and verify that `make check` runs smoothly, before you submit.

**User Identification**

* The first line of the makefile should be `# id` where `id` is the ID-number of the submitting student.
* The second line of the makefile should be `# user` where `user` is the biu user names 
of the submitting student.
* Make sure to have at least one space between the `#` and the `id` or `user`.



### An example

Assuming we had assignment number 99, with two task: in task 1 your program was requested to print "Hello Task1", and in task 2 your program was requested to print "Hello Task2". Then the following file is a valid submission: [ass99.zip](data/ass99.zip)

### A note on build systems

Note that `make` is not the preferred way to build java projects. We chose to use `make` because you are familiar with it from the intro course. The standard build tools for java are [ant](http://ant.apache.org/) and [maven](http://maven.apache.org/).

Those of you who prefer to use `ant` instead of `make` are encouraged to do so. Just supply the appropriate build file. Even if you do use `ant` you should also supply a makefile.
The makefile will include:

a) The `# id` and `# user` lines as described above.

b) The `compile` and `run` targets as above, where each target will call the corresponding ant target, for example:
```makefile
# 123898098
# user
compile:
   ant compile

run1:
   ant run1

```

