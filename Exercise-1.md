## Classes and Objects

This Exercise is not mandatory. *You do not need to submit it, and you will not be graded for it*. However, you are encouraged to do it as we believe it will improve your understanding of the material.

### Estimating The Quality of the Monte-Carlo Estimation

In the lecture, you saw a program that uses a Monte-Carlo method to estimate what PI is.
The code is available [here](http://www.cs.biu.ac.il/~89-111/code/montecarlopi.zip)

The estimation method relies on random numbers -- every time you run it, you the calculation is somewhat different.

### Task 1:
**How stable is the process?** In other words, if we run the program 20 times, how much different will our numbers be? And how does this relate to the number of random points we use for the estimation?

Your task is to write a program to find out.

Let's define the `spread` for a method using K random points to be the difference between the maximum and the minimum estimated values of PI, when we run the estimation for N times. 
Write a class, `MonteCarloSpreadEstimator`, whose job is to calculate the spread of the method when using 10, 100, 1000, 10000, 100000 and 1000000 random points, when running 20 estimations.
The output should be 6 lines, where each line lists the number of points and the spread achieved using this number of points.

You should use the existing classes to do so. Notice how easy it is to use a class in a different context when the algorithm is inside an object method and not inside main!

### Task 2:

Now, you should print in each output line not just the spread, but also the number of points that fell inside the circle.  Modify the code to support this.
You may need to change some of the existing classes.
