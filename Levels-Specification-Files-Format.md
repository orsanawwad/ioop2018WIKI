### General
In both block and level definition files you should ignore empty lines and lines that start with `#` (used to write comments). 

### Block Definitions File Format

The block definitions file maps characters to spacing elements or block-information.

An example of a block definitions file is given below. An explanation follows.

```
# default values for blocks
default height:20 

# block definitions
bdef symbol:a width:20 hit_points:1 fill:color(RGB(10,10,10)) stroke:color(black)
bdef symbol:b width:20 hit_points:2 fill:color(RGB(10,10,10)) 
bdef symbol:c width:20 hit_points:3 fill:color(red) fill-2:color(cyan) stroke:color(blue)
bdef symbol:n width:20 hit_points:4 fill:image(filename.png) stroke:color(black)

# spacers definitions
sdef symbol:* width:20
sdef symbol:^ width:10
```

We have four types of lines in the file:

* **comments or blanks** lines are ignored.
* **block definitions** lines begin with the token `bdef`, followed by a space-separated list of properties. Each property has the form `key:value`.
* **default values** a single line beginning with the token `default`, followed by a space-separated list of properties. Each property has the form `key:value`. You can assume that if it exists it will be above the `bdef` lines.
* **spacer definitions** lines begin with the token `sdef`, followed by a space-separated list of properties. Each property has the form `key:value`

Each block definition has the following properties:
* `symbol`: the character which is used to represent the block type in the levels-information file. Must be a single character.
* `height`: the height of the block, in pixels.  Must be positive integer.
* `width`: the width of the block, in pixels. Must be a positive integer.
* `hit_points`: number of hit-points of the block. Must be a positive integer.
* `fill`: The block should be filled using one of the following value formats:
   * `color(colorname)`, where `colorname` is one of `black, blue, cyan, gray, lightGray, green, orange, pink, red, white, yellow`. The colors correspond to the static public fields that are found on the `Color` class.
   * `color(RGB(x,y,z))`, where `x`, `y`, `z` are integers. The block should have the `RGB` color specified by the given `x`, `y` and `z` components.
   * `image(filaneme.png)`, where `filename.png` is the name of a file. In this case, the blocks appearance should be taken from the given file. (The image is loaded at the position of the block, no resizing or clipping is required, it's assumed the image size matches the size of the definition)
* `fill-k`: if the number of hit-points for a block is > 1, we can specify a different appearance for each number of remaining hit-points.  In this case, `fill-2` will be the fill to use when the block has 2 hit-points.  If a fill for hit-point number `i` is not explicitly specified, the `fill` should be used instead. (`fill-1` is also legal and should be supported)
* `stroke`: The blocks border should be drawn using one of the following value formats (if this property is missing, no border should be drawn):
    * `color(colorname)`, see definition above (same as with fill).
    * `color(RGB(x,y,z))`, see definition above (same as with fill).

A block is defined on a `bdef` line. Each `bdef` line specifies one block type, and lists its properties. Some properties may be missing. In this case, their values are taken from the `default` line. If a required property is not defined and there is no default for it the parsing process should fail.

A spacer-definition line has two properties (it does not inherit default definitions):
* `symbol`: the character which is used to represent the space-type in the levels-information file. Must be a single character.
* `width`: the width of the spacing element in pixels. Must be a positive integer.

**HINT: Displaying an Image file**
```java
import biuoop;
// load the image data into an java.awt.Image object
Image img = null;
try {
    img = ImageIO.read(new File("strawberry.jpg"));
} catch (IOException e) {
    // ...
}
// Draw the image on a DrawSurface 
GUI gui = new GUI(...);
DrawSurface d = gui.getDrawSurface();
d.drawImage(10, 20, img); // draw the image at location 10, 20.
gui.show(d);
```


### Levels Information Specification File Format

The file contains a sequence of level specifications.
Each level specification begins with the line `START_LEVEL`
and ends with the line `END_LEVEL`.

Below is an example of a levels information file with two levels.


```
START_LEVEL
level_name:first level
ball_velocities:45,200 -45,200 0,300
background:image(image1.png)
paddle_speed:650
paddle_width:160
block_definitions:blocks1.txt
blocks_start_x:25
blocks_start_y:80
row_height:25
num_blocks:10
START_BLOCKS
*
*
bbbbbbbbbb
END_BLOCKS
END_LEVEL

START_LEVEL
background:color(red)
ball_velocities:30,400
level_name:second level
paddle_speed:450
paddle_width:240
block_definitions:blocks2.txt
blocks_start_x:25
blocks_start_y:80
row_height:50
num_blocks:5
START_BLOCKS
*
*
bbbbaaaaaa
nnn****nnn
*
bbbbbbbbbb
END_BLOCKS
END_LEVEL
```

Each level includes the following fields, as well as a BLOCKS section, which will be described below. 

* `level_name` specified the name of the level.
* `ball_velocities` specifies the ball velocities. This field is a space-separated list of items, each item is of the form `a,s` where `s` is the speed and `a` is the angle.
* `background` specifies the level's background. The format of its value is exactly the same as for the `fill` property when defining a block so it should support the two color formats and the image format.
* `paddle speed` specifies the paddle speed.
* `paddle width` specifies the paddle width.
* `block_definitions` specifies the file from which block definitions are read.
* `blocks_start_x` the blocks layout horizontal starting point, or the `x` value of the first block in every column.
* `blocks_start_y` the block layout vertical starting point, or the `y` value of the blocks on the first row.
* `row_height` the blocks are arranged in equally-spaced rows. This field specifies the number of pixels in each row. If first row `y` value is 50 (`blocks_start_y:50`), the second row will be located at `50` + the value of row_height. (Notice that we can have blocks with bigger or smaller values of height, you don't need to do any special treatment for this scenario)
* `num_blocks` the number of blocks that need to be destroyed in order to pass this level.

The fields can appear in any order. Missing fields are a problem. In case of a missing field, your program should recognize the error and fail the parsing process.

The **block layout** starts with a `START_BLOCKS` line and ends with an `END_BLOCKS` line. 
It will always appear at the end of the level information. 
Within the block layout, each line (non empty) specifies one row of blocks, from row 0 downwards.
A line with only spacer(s) means an empty row. 
Within each line, each character corresponds to either a block or an horizontal space, as defined in the blocks definition file.

**A note on file locations**
All the file names specified in the levels and block definition files should be relative to the class path. The reason we want them to be relative to the class path is that later we will be able to read the files from inside a `jar`, something we can not do with regular File references.

To get an input stream relative to the class path (even if it's inside a `jar`), use the following:
```java
InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("image.png");
```

The idea is to keep a folder with files(definitions and images) and then add that folder to the class path when running the JVM:
```
java -cp bin:resources ... 
```

If you don't add the `resources` folder to you class path you wont be able to load them with the command from above.

**Example specification:**

Complete examples can be found here: [[Level-Specification-examples]]