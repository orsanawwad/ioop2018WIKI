# Makefiles

`make` is a program that helps build large projects. It has many features. We will be using a very very minimal set of its features in this course.

When you type `make`, you invoke them `make` program, which looks for a file called `makefile` or `Makefile` in the current directory.
If it finds such a file, it reads it and starts following the instructions within it.

The format of the makefile is as follows:
```makefile
# comment lines start with the hash sign (#)

target1:
    command_1
    command_2

target2:
    command_3

target3: target2
    command_4

```

There are lines that begin with `word:`. These are called targets. The lines below them start with a tab (real tab, not spaces) and then a command.
When `make` runs, it looks for the first target in the file (in the case aove, `target1`) and executes the commands below it one by one (in the case above, it will execute `command_1` and `command_2`). After executing these commands, it quits.

You can also specify a specific target as argument to make. For example, if we invoke make with `make target2`, it will look for the target `target2` and invoke the command below it (`command_3`).

We can also say `make target2 target1` and it will invoke both of them.

Note that the last target in the example (`target3`) has also words after the `:`. These are called _dependencies_. When you invoke a target, it will first invoke all of its dependencies, and then its commands. So if we run `make target3` it will run `command_3` followed by `command_4`.

In our case, we use make to build and run java programs, so the commands will be commands to compile java programs (lines starting with `javac`) or to run java programs (lines starting with `java`).

