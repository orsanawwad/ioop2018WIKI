## Install Java

Install the Java Development Kit. ([JDK](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html))
Download the installation file from [here](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and run it.

## Make sure that your installation is working

Open up a terminal window, and issue the following commands:

```
java -version

javac -version
```

You should see output similar to:
```
java version "1.8.0_74"
Java(TM) SE Runtime Environment (build 1.8.0_74-b02)
Java HotSpot(TM) Server VM (build 25.74-b02, mixed mode)
```

and
```
javac 1.8.0_74
```

## Write, Compile and Run a simple program

1. Create a file named `HelloWorld.java`
2. Enter the following content:
```java
public class HelloWorld {
  public static void main(String[] args) {
    for (int i = 0; i < 5; i++) {
       System.out.println("Hi");
    }
  }
}
```
3. Compile the program using: `javac HelloWorld.java`
4. Verify that a corresponding `HelloWorld.class` file is created.
5. Run the compiled program: `java HelloWorld`

You should see the output
```
Hi
Hi
Hi
Hi
Hi
```

## Make sure you know about the Coding Style

Read and follow the instructions in the [[CodingStyle]] page. 
