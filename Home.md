## Introduction to Object Oriented Programming 2018
### Bar Ilan University

**Instructor**: Dr. Yoav Goldberg

**TAs**:<br> 
Roee Aharoni<br>
Eyal Dayan<br>
Mor Sinay<br>

### About

This course intends to teach the basic of object-oriented programming, through programming in the Java programming language.

### Course Material
Available [here](https://piazza.com/biu.ac.il/spring2018/oop/resources)

### Optional Reading

* [The Java Trail / Tutorials](http://docs.oracle.com/javase/tutorial/reallybigindex.html)
* [Java Language Specification](materials/jls3.pdf)
